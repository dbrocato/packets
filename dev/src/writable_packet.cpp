#include "packets.h"

#include <cstring>

WritablePacket::WritablePacket(int capacity)
{
    this->capacity = capacity;
    this->size = 0;

    this->bytes = new unsigned char[capacity];
    memset(this->bytes, 0, capacity);
}

WritablePacket::~WritablePacket()
{
    delete[] bytes;
}

unsigned char* WritablePacket::GetByteArray() { return bytes; }

int WritablePacket::GetSize() { return size; }
int WritablePacket::GetCapacity() { return capacity; }

void WritablePacket::SetCapacity(int new_capacity)
{
    if (capacity == new_capacity) return;

    unsigned char* array = new unsigned char[new_capacity];

    if (new_capacity < size) size = new_capacity;
    
    memcpy(array, bytes, size);

    delete[] bytes;
    bytes = array;
    capacity = new_capacity;
}

bool WritablePacket::CanFit(int addition_size) { return size + addition_size <= capacity; }

void WritablePacket::ShrinkToFit() { SetCapacity(size); }

/* Numeric writes as one-liners that call private helper function. */

bool WritablePacket::Write(char data) { return WriteNumeric<char>(data); }
bool WritablePacket::Write(unsigned char data) { return WriteNumeric<unsigned char>(data); }

bool WritablePacket::Write(short data) { return WriteNumeric<short>(data); }
bool WritablePacket::Write(unsigned short data) { return WriteNumeric<unsigned short>(data); }

bool WritablePacket::Write(int data) { return WriteNumeric<int>(data); }
bool WritablePacket::Write(unsigned int data) { return WriteNumeric<unsigned int>(data); }

bool WritablePacket::Write(long data) { return WriteNumeric<long>(data); }
bool WritablePacket::Write(unsigned long data) { return WriteNumeric<unsigned long>(data); }

bool WritablePacket::Write(float data) { return WriteNumeric<float>(data); }
bool WritablePacket::Write(double data) { return WriteNumeric<double>(data); }

bool WritablePacket::Write(bool data) { return WriteNumeric<bool>(data); }

bool WritablePacket::Write(std::string data)
{
    int data_size = data.length();

    // Assert that there is enough space for the size indicator of the string and the string itself.
    if (!CanFit(sizeof(int) + data_size)) return false;

    // Write string length. This if-return is redundant, but is good practice.
    if (!Write(data_size)) return false;

    // Write data.
    memcpy(&bytes[size], data.c_str(), data_size);

    size += data_size;

    return true;
}

/* Writes uncompressed numeric types, such as int32, int64, float, double, etc.. */
template <class T>
bool WritablePacket::WriteNumeric(T data)
{
    int data_size = sizeof(T);
    if (!CanFit(data_size)) return false;

    memcpy(&bytes[size], reinterpret_cast<unsigned char*>(&data), data_size);

    size += data_size;

    return true;
}