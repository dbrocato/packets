#include "packets.h"

#include <typeinfo>
#include <type_traits>
#include <cstring>

ReadablePacket::ReadablePacket(unsigned char* bytes, int size)
{
    this->size = size;
    this->cursor = 0;

    /* Make internal copy of byte array. Promotes thread safety between the thread
    that creates the ReadablePacket and the thread that handles it. */
    this->bytes = new unsigned char[size];
    memset(this->bytes, 0, size);
    memcpy(this->bytes, bytes, size);
}

ReadablePacket::~ReadablePacket()
{
    delete[] bytes;
}

int ReadablePacket::GetSize() { return size; }

bool ReadablePacket::Seek(int index)
{
    if (index < 0 || index > size) return false;
    cursor = index;
    return true;
}

bool ReadablePacket::HasNext() { return cursor != size; }

void ReadablePacket::Read(char& out) { out = ReadNumeric<char>(); }
void ReadablePacket::Read(unsigned char& out) { out = ReadNumeric<unsigned char>(); }

void ReadablePacket::Read(short& out) { out = ReadNumeric<short>(); }
void ReadablePacket::Read(unsigned short& out) { out = ReadNumeric<unsigned short>(); }

void ReadablePacket::Read(int& out) { out = ReadNumeric<int>(); }
void ReadablePacket::Read(unsigned int& out) { out = ReadNumeric<unsigned int>(); }

void ReadablePacket::Read(long& out) { out = ReadNumeric<long>(); }
void ReadablePacket::Read(unsigned long& out) { out = ReadNumeric<unsigned long>(); }

void ReadablePacket::Read(float& out) { out = ReadNumeric<float>(); }
void ReadablePacket::Read(double& out) { out = ReadNumeric<double>(); }

void ReadablePacket::Read(bool& out) { out = ReadNumeric<bool>(); }

void ReadablePacket::Read(std::string& out)
{
    int data_size = ReadNumeric<int>();
    out = std::string(reinterpret_cast<char*>(&bytes[cursor]), data_size);
    cursor += data_size;
}

void ReadablePacket::Read(unsigned char* out, int data_size)
{
    memcpy(out, &bytes[cursor], data_size);
    cursor += data_size;
}

template <class T>
T ReadablePacket::ReadNumeric()
{
    T val = *reinterpret_cast<T*>(&bytes[cursor]);
    cursor += sizeof(T);
    return val;
}