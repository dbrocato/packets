#ifndef PACKETS_H
#define PACKETS_H

#include <string>
#include <list>

class WritablePacket
{
    public:

        WritablePacket(int capacity);
        ~WritablePacket();

    private:

        unsigned char* bytes;
        int capacity;
        int size;

    public:

        /* Returns a direct pointer to the memory array used internally by
        the packet to sequentially store individual bytes. */
        unsigned char* GetByteArray();

        /* Returns the size of this packet as represented by the number
        of bytes currently stored in the byte array. */
        int GetSize();

        /* Returns the static capacity of this packet as represented by
        the allocated size of the byte array. */
        int GetCapacity();

        /* Resizes the static capacity of this packet to reflect new_capacity.
        If new_capacity is smaller than the current size of the packet, the
        packet will "shrink" to be the size of new_capacity, and all data
        after new_capacity bytes will be lost. */
        void SetCapacity(int new_capacity);
        
        bool CanFit(int addition_size);

        void ShrinkToFit();

        void Clear();

        bool Write(char data);
        bool Write(unsigned char data);

        bool Write(short data);
        bool Write(unsigned short data);

        bool Write(int data);
        bool Write(unsigned int data);

        bool Write(long data);
        bool Write(unsigned long data);

        bool Write(float data);
        bool Write(double data);

        bool Write(bool data);

        /* Writes a non-null-terminated string named data to the packet in the following format:
            <length of data><data>
        NOTE: Behavior undefined if string is not ASCII. */
        bool Write(std::string data);
    
    private:

        /* Template function as a shortcut to write numerics. */
        template <class T>
        bool WriteNumeric(T data);
};

class ReadablePacket
{
    public:

        /* Constructor takes a byte array and its size and makes an internal
        copy of the array for use in this ReadablePacket object. */
        ReadablePacket(unsigned char* bytes, int size);
        ~ReadablePacket();

        /* Returns the size of the packet as represented by the size of the
        byte array that was initially passed in. */
        int GetSize();

        /* Moves the cursor to the given index of the byte array. Returns true
        if the seek was successful. Returns false if the seek failed because
        the given index was out of bounds. */
        bool Seek(int index);

        /* Returns true if there is still content to be read in the packet, or
        false otherwise. */
        bool HasNext();

        void Read(char& out);
        void Read(unsigned char& out);
        
        void Read(short& out);
        void Read(unsigned short& out);

        void Read(int& out);
        void Read(unsigned int& out);

        void Read(long& out);
        void Read(unsigned long& out);

        void Read(float& out);
        void Read(double& out);

        void Read(bool& out);
        
        void Read(std::string& out);

        void Read(unsigned char* out, int data_size);

    private:

        template <class T>
        T ReadNumeric();

        unsigned char* bytes;
        int size;
        int cursor;
};

#endif